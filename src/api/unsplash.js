import axios from "axios";

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 3VqnZVAqrWYViKbA_C62Y55UAeHdLjdcDGfZSq32D5Q',
    }
});